angular.module('index').controller('createStationModalController', function($scope, $uibModalInstance, location, UnitService, StationService) {
  $scope.station = {
    location: location
  };

  $scope.searchUnits = function(term) {
    return UnitService.searchUnit(term);
  }

  $scope.ok = function() {
    StationService.createStation($scope.station, function(err, data) {
      $scope.addingUnit = false;
      $scope.addingStation = false;
      $uibModalInstance.close(data);
    });
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});
