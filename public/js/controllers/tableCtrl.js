angular.module('index').controller('tableCtrl', function($scope, $timeout, $uibModal, UnitService, StationService) {
  $scope.showModal = false;
  $scope.station = [];
  $scope.createUnit = function() {
    var modalInstance = $uibModal.open({
      templateUrl: '/html/templates/create_unit_modal_table.html',
      controller: 'createUnitModalController',
      resolve: {
        location: function() {
          return {};
        }
      }
    });
    modalInstance.result.then(function(unit) {
      loadtable(currenting);
    });
  };

  $scope.detail = function(u) {
    loadStation(u.id, currentingStation)
  };

  $scope.openEdit = function(u) {
    UnitService.findUnitById(u.parentId, function(err, parent) {
      var modalInstance = $uibModal.open({
        templateUrl: '/html/templates/edit_unit_modal_table.html',
        controller: 'editUnitModalControllerTable',
        resolve: {
          location: function() {
            return {
              id: u.id,
              lat: u.latitude,
              lng: u.longitude,
              name: u.name,
              parent: parent
            };
          }
        }
      });
      modalInstance.result.then(function() {
        loadtable(currenting);
      });
    })
  };
  $scope.deleteUnit = function(u) {
    UnitService.deleteUnit(u.id, function(err, data) {
      loadtable(currenting);
    });
  }
  $scope.deleteStation = function(i) {
    StationService.deleteStation(i.id, function(err, data) {
      loadStation(i.unitId, currentingStation);
    });
  }
  $scope.editStation = function(i) {
    UnitService.findUnitById(i.unitId, function(err, parent) {
      var modalInstance = $uibModal.open({
        templateUrl: '/html/templates/edit_station_modal_table.html',
        controller: 'editStationModalTableController',
        resolve: {
          location: function() {
            return {
              id: i.id,
              lat: i.latitude,
              lng: i.longitude,
              name: i.name,
              parent: parent
            };
          }
        }
      });
      modalInstance.result.then(function() {
        loadStation(i.unitId, currentingStation);
      });
    })
  };
  $scope.createStation = function() {
    var modalInstance = $uibModal.open({
      templateUrl: '/html/templates/create_station_modal_table.html',
      controller: 'createStationModalController',
      resolve: {
        location: function() {
          return {};
        }
      }
    });
    modalInstance.result.then(function(unit) {
      alert('Thêm Trạm thành công !!');
      loadStation(unit.unitId, currentingStation);
    });
  };
  var currentingStation = 0;
  $scope.currentingStationId = 0;
  $scope.numPerPageStation = 2;
  $scope.currentPageStation = 1;
  $scope.maxSizeStation = 5;

  function loadStation(u, offset) {
    StationService.getStationsByUnitId(u, function(data) {
      $scope.totalItemsStation = (data.length / $scope.numPerPageStation) * 10;
      StationService.getStationPaging(u, offset, $scope.numPerPageStation, function(data) {
        $scope.station = data;
      });
     $scope.currentingStationId = u;
    });
  };
  $scope.pageChangedStation = function(currentingStationId) {
    $scope.offsetSta = ($scope.currentPageStation - 1) * ($scope.numPerPageStation);
    StationService.getStationPaging($scope.currentingStationId, $scope.offsetSta, $scope.numPerPageStation, function(data) {
      $scope.station = data;
    })
    currentingStation = $scope.offsetSta;
  };
  $scope.setPage = function() {
    $scope.offset = ($scope.currentPage - 1) * ($scope.numPerPage);
    UnitService.getUnitPaging($scope.offset, $scope.numPerPage, function(data) {
      $scope.data = data
    });
    currenting = $scope.offset;
  };
  $scope.maxSize = 5;
  $scope.currentPage = 1;
  $scope.numPerPage = 5;
  var currenting = 0;
  loadtable(currenting);

  function loadtable(offset) {
    UnitService.getUnit(function(data) {
      $scope.totalItems = (data.length / $scope.numPerPage) * 10;
      UnitService.getUnitPaging(offset, $scope.numPerPage, function(data) {
        $scope.data = data
      });
    })
  };

});
