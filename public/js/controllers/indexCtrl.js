﻿angular.module('index').controller('indexCtrl', function($scope, $timeout, $uibModal, leafletData, UnitService, StationService) {
  $scope.markers = {};
  $scope.showModal = false;
  $scope.modalTitle = "Tạo mới";

  $scope.addingUnit = false;
  $scope.addingStation = false;
  $scope.editing = true;
  $scope.deleting = false;

  $scope.sediting = true;
  $scope.sdeleting = false;

  $scope.$on('leafletDirectiveMarker.dragend', function(event, data) {
    $scope.markers[data.modelName].lat = data.model.lat;
    $scope.markers[data.modelName].lng = data.model.lng;

    if (data.modelName == "currentUnitMarker") {
      for (var i in $scope.markers) {
        $scope.markers[i].focus = false;
      };
      $scope.markers.currentUnitMarker.focus = true;
      $scope.markers.currentUnitMarker.lng = data.model.lng;
      $scope.markers.currentUnitMarker.lat = data.model.lat;
      $scope.editing = false;
    } else if (data.modelName == "currentStationMarker") {
      for (var i in $scope.markers) {
        $scope.markers[i].focus = false;
      };
      $scope.markers.currentStationMarker.focus = true;
      $scope.markers.currentStationMarker.lng = data.model.lng;
      $scope.markers.currentStationMarker.lat = data.model.lat;
      $scope.sediting = false;
    }
  });

  function getNodeById(id, scope) {
    var root = false;
    if (!scope) {
      scope = angular.element(document.getElementById("tree-root")).scope();
      root = true;
    }
    if (!root && scope.$modelValue && scope.$modelValue.id == id) {
      return scope;
    }
    var childNodes = [];
    if (root) {
      childNodes = scope.$nodesScope.childNodes();
    } else {
      childNodes = scope.childNodes();
    }
    if (!childNodes) {
      return null;
    }
    for (var i = 0; i < childNodes.length; i++) {
      var scope = childNodes[i];
      if (scope) {
        var result = getNodeById(id, scope);
        if (result) {
          return result;
        }
      }
    };
    return null;
  };

  $scope.data = [];

  function expandTree(unit) {
    var units = unit.ancestors;
    if (units.length <= 0) {
      $scope.data.push(unit);
      return;
    }
    units.push(unit);

    (function expandNext() {
      if (units.length > 0) {
        var toLoad = units.shift();
        UnitService.getChildren(toLoad.id, function(err, result) {
          var scope = getNodeById(toLoad.id);
          if (!scope) {
            return;
          }
          scope.$modelValue.children = result;
          scope.expand();
          expandNext();
        });
      }
    })();
  }

  $scope.load = function(scope, cb) {
    if (scope.collapsed) {
      UnitService.getChildren(scope.node.id, function(err, result) {
        scope.$modelValue.children = result;
        scope.toggle();
        if (cb) {
          cb();
        }
      });
    } else {
      scope.toggle();
    }
  };

  function showLocationMarker() {
    $scope.markers.location = {
      lng: $scope.center.lng,
      lat: $scope.center.lat,
      draggable: true
    };
  }

  $scope.addUnit = function() {
    showLocationMarker();
    $scope.addingUnit = true;
  }

  $scope.addStation = function() {
    showLocationMarker();
    $scope.addingStation = true;
  }

  $scope.cancelAdding = function() {
    $scope.addingUnit = false;
    $scope.addingStation = false;

    delete $scope.markers.location;
  }

  $scope.confirmLocation = function() {
    if ($scope.addingUnit) {
      var modalInstance = $uibModal.open({
        templateUrl: '/html/templates/create_unit_modal.html',
        controller: 'createUnitModalController',
        resolve: {
          location: function() {
            return {
              lat: $scope.markers.location.lat,
              lng: $scope.markers.location.lng,
            };
          }
        }
      });
      modalInstance.result.then(function(unit) {
        $scope.cancelAdding();
        expandTree(unit);
      });

    } else if ($scope.addStation) {
      var modalInstance = $uibModal.open({
        templateUrl: '/html/templates/create_station_modal.html',
        controller: 'createStationModalController',
        resolve: {
          location: function() {
            return {
              lat: $scope.markers.location.lat,
              lng: $scope.markers.location.lng,
            };
          }
        }
      });
      modalInstance.result.then(function(station) {
        $scope.cancelAdding();
      });
    }
  };

  angular.extend($scope, {
    center: {
      lng: 105.802,
      lat: 21.023,
      zoom: 13
    },
    default: {
      scrollWheelZoom: true,
    },
    events: {
      map: {
        enable: ['zoomstart', 'drag', 'click', 'mousemove', 'dragend', 'drag'],
        logic: 'emit'
      }
    }
  });

  UnitService.getChildren(0, function(err, result) {
    $scope.data = result;
  });

  $scope.loadStationTable = function(unit) {

    $scope.currentUnit = unit;

    $scope.markers.currentUnitMarker = {
      lat: unit.latitude,
      lng: unit.longitude,
      focus: true,
      message: "<div ng-include src=\"'/html/templates/unit_marker_popup_template.html'\"></div>",
      getMessageScope: function() {
        return $scope;
      }
    };

    $scope.center.lat = unit.latitude;
    $scope.center.lng = unit.longitude;

    StationService.getStationsByUnitId(unit.id, function(stations) {
      $scope.stations = stations;
    });
  };

  $scope.subscribeList = {};

  function fitBound() {
    var locs = [];
    for (var i in $scope.subscribeList) {
      var station = $scope.subscribeList[i];
      locs.push([station.latitude, station.longitude]);
    }
    if (locs.length > 0) {
      leafletData.getMap().then(function(map) {
        map.fitBounds(locs);
      });
    }

  }

  $scope.fitBound = fitBound;

  $scope.addToSubscribe = function(station) {
    $scope.subscribeList[station.id] = station;
    $scope.markers[station.id] = {
      lat: station.latitude,
      lng: station.longitude
    };
    fitBound();
  };

  $scope.removeFromSubscribe = function(station) {
    delete $scope.subscribeList[station.id];
    delete $scope.markers[station.id];
    fitBound();
  }

  $scope.loadStationLoc = function(station) {
    $scope.currentStation = station;

    $scope.markers.currentStationMarker = {
      lat: station.latitude,
      lng: station.longitude,
      focus: true,
      message: "<div ng-include src=\"'/html/templates/station_marker_popup_template.html'\"></div>",
      getMessageScope: function() {
        return $scope;
      }
    };

    $scope.center.lat = station.latitude;
    $scope.center.lng = station.longitude;
  }

  $scope.editUnitLocation = function(unit) {
    $scope.markers.currentUnitMarker.draggable = true;
    $scope.markers.currentUnitMarker.focus = false;
    $scope.editing = false;
    // $scope.markers.currentUnitMarker.message = "<div ng-include src=\"'/html/templates/confirm_edit_unit_location.html'\"></div>";
  }

  $scope.deleteUnitObj = function() {
    $scope.deleting = true;
  }

  $scope.updateUnitLocation = function(unitId) {
    UnitService.updateUnitLocation({
      id: unitId,
      lng: $scope.markers.currentUnitMarker.lng,
      lat: $scope.markers.currentUnitMarker.lat
    }, function(err, unit) {
      var node = getNodeById(unit.id);
      node.$modelValue.latitude = unit.latitude;
      node.$modelValue.longitude = unit.longitude;
      $scope.markers.currentUnitMarker = {
        lat: unit.latitude,
        lng: unit.longitude,
        focus: false,
        message: "<div ng-include src=\"'/html/templates/unit_marker_popup_template.html'\"></div>",
        getMessageScope: function() {
          return $scope;
        }
      };
      $scope.editing = true;
    });
  }

  $scope.deleteUnit = function(unitId) {
    UnitService.deleteUnit(unitId, function(err, data) {
      var node = getNodeById(unitId);
      node.remove();
      $scope.deleting = false;
      delete $scope.markers.currentUnitMarker;
    });
  };

  $scope.cancelAction = function(unit) {
    $scope.markers.currentUnitMarker.focus = false;

    if ($scope.deleting == true) {
      $scope.deleting = false;
    } else if ($scope.editing == false) {
      $scope.markers.currentUnitMarker.lng = unit.longitude;
      $scope.markers.currentUnitMarker.lat = unit.latitude;
      $scope.markers.currentUnitMarker.draggable = false;
      $scope.editing = true;

    }
  }

  $scope.editUnitInfo = function(unit) {
    UnitService.findUnitById(unit.parentId, function(err, parent) {
      var modalInstance = $uibModal.open({
        templateUrl: '/html/templates/edit_unit_modal.html',
        controller: 'editUnitModalController',
        resolve: {
          location: function() {
            return {
              id: unit.id,
              lat: unit.latitude,
              lng: unit.longitude,
              name: unit.name,
              parent: parent
            };
          }
        }
      });

      modalInstance.result.then(function(updatedUnit) {
        $scope.cancelAdding();
        if (unit.parentId != updatedUnit.parentId) {
          var node = getNodeById(unit.id);
          node.remove();
        }
        expandTree(updatedUnit);
      });
    });
  }

  $scope.deleteStationObj = function() {
    $scope.deleting = true;
  }

  function reloadStations(unitId) {
    StationService.getStationsByUnitId(unitId, function(stations) {
      $scope.stations = stations;
    });
  }

  $scope.deleteStation = function(station) {
    StationService.deleteStation(station.id, function(err, data) {
      reloadStations(station.unitId);
      $scope.deleting = false;
      delete $scope.markers.currentStationMarker;
    });
  }

  $scope.editStationInfo = function(station) {
    UnitService.findUnitById(station.unitId, function(err, parent) {
      var modalInstance = $uibModal.open({
        templateUrl: '/html/templates/edit_station_modal.html',
        controller: 'editStationModalController',
        resolve: {
          location: function() {
            return {
              id: station.id,
              lat: station.latitude,
              lng: station.longitude,
              name: station.name,
              parent: parent
            };
          }
        }
      });

      modalInstance.result.then(function(updatedStation) {
        $scope.loadStationTable(updatedStation.unit);
        $scope.loadStationLoc(updatedStation);
        if ((updatedStation.unit.id != station.unitId) && updatedStation.unit.ancestors.length > 0) {
          expandTree(updatedStation.unit);
        }
      });
    });
  }

  $scope.editStationLocation = function(station) {
    $scope.markers.currentStationMarker.draggable = true;
    $scope.markers.currentStationMarker.focus = false;
    $scope.sediting = false;
  }

  $scope.cancelStationAction = function(station) {
    $scope.markers.currentStationMarker.focus = false;

    if ($scope.sdeleting == true) {
      $scope.sdeleting = false;
    } else if ($scope.sediting == false) {
      $scope.markers.currentStationMarker.lng = station.longitude;
      $scope.markers.currentStationMarker.lat = station.latitude;
      $scope.markers.currentStationMarker.draggable = false;
      $scope.sediting = true;

    }
  }

  $scope.updateStationLocation = function(stationId) {
    StationService.updateStationLocation({
      id: stationId,
      lng: $scope.markers.currentStationMarker.lng,
      lat: $scope.markers.currentStationMarker.lat
    }, function(err, station) {

      for (var i = 0; i < $scope.stations.length; i++) {
        if ($scope.stations[i].id == station.id) {
          $scope.stations[i].latitude = station.latitude;
          $scope.stations[i].longitude = station.longitude;
        }
      }

      $scope.markers.currentStationMarker = {
        lat: station.latitude,
        lng: station.longitude,
        focus: false,
        message: "<div ng-include src=\"'/html/templates/station_marker_popup_template.html'\"></div>",
        getMessageScope: function() {
          return $scope;
        }
      };
      $scope.sediting = true;
    });
  }
});
