angular.module('index').controller('editStationModalController', function($scope, $uibModalInstance, location, UnitService, StationService) {

  $scope.station = {
    location: location
  };

  $scope.searchUnits = function(term) {
    return UnitService.searchUnit(term);
  }

  $scope.ok = function() {
    StationService.updateInfo({
      id: $scope.station.location.id,
      name: $scope.station.location.name,
      unitId: $scope.station.location.parent.id
    }, function(err, result) {
      $uibModalInstance.close(result);
    });
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});
