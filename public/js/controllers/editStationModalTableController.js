angular.module('index').controller('editStationModalTableController', function($scope, $uibModalInstance, location, UnitService, StationService) {

  $scope.station = {
    location: location
  };

  $scope.searchUnits = function(term) {
    return UnitService.searchUnit(term);
  }
  $scope.ok = function() {
    StationService.updateStadionTable({
      id: $scope.station.location.id,
      name: $scope.station.location.name,
      unitId: $scope.station.location.parent.id,
      latitude:$scope.station.location.lat,
      longitude:$scope.station.location.lng
    }, function(err, result) {
      $uibModalInstance.close(result);
    })
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});
