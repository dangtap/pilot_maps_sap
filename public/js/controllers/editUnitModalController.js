angular.module('index').controller('editUnitModalController', function($scope, $uibModalInstance, location, UnitService) {

  $scope.unit = {
    location: location
  };

  $scope.searchUnits = function(term) {
    return UnitService.searchUnit(term);
  }

  $scope.ok = function () {
    if($scope.unit.parent) {
      $scope.unit.parentId = $scope.unit.parent.id;
    }

    UnitService.updateInfo({
      id: $scope.unit.location.id,
      name: $scope.unit.location.name,
      parentId: $scope.unit.location.parent.id
    }, function(err, result) {
      $uibModalInstance.close(result);
    });
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});
