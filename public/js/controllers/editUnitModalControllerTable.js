angular.module('index').controller('editUnitModalControllerTable', function($scope, $uibModalInstance, location, UnitService) {

  $scope.unit = {
    location: location
  };

  $scope.searchUnits = function(term) {
    return UnitService.searchUnit(term);
  }

  $scope.ok = function() {
    if ($scope.unit.parent) {
      $scope.unit.parentId = $scope.unit.parent.id;
    }
    UnitService.updateUnitTable({
      id: $scope.unit.location.id,
      name: $scope.unit.location.name,
      parentId: $scope.unit.location.parent.id,
      latitude: $scope.unit.location.lat,
      longitude: $scope.unit.location.lng
    }, function(err, result) {
      $uibModalInstance.close(result);
    });
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});
