angular.module('index').controller('createUnitModalController', function($scope, $uibModalInstance, location, UnitService) {

  $scope.unit = {
    location: location
  };

  $scope.searchUnits = function(term) {
    return UnitService.searchUnit(term);
  }

  $scope.ok = function () {
    if($scope.unit.parent) {
      $scope.unit.parentId = $scope.unit.parent.id;
    }
    UnitService.createUnit($scope.unit, function(err, result) {
      $uibModalInstance.close(result);
    });
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});
