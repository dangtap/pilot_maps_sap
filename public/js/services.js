angular.module('index').service('UnitService', function($http, Upload) {
  return {
    getUnitTree: function(cb) {
      $http.get('/units').then(function(res) {
        cb(res.data);
      });
    },
    getUnitPaging: function(offset, limit, cb) {
      $http.get('/units/paging/' + offset + '/' + limit).then(function(res) {
        cb(res.data);
      });
    },
    getUnit: function(cb) {
      $http.get('/units/noHierarchy').then(function(res) {
        cb(res.data);
      });
    },
    searchUnit: function(term) {
      return $http.get('/units/search/' + term).then(function(res) {
        return res.data;
      });
    },
    getChildren: function(id, cb) {
      $http.get('/units/children/' + id).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    createUnit: function(data, cb) {
      Upload.upload({
        url: '/units/create',
        data: data
      }).then(function(res) {
        cb(false, res.data);
      }, function(err) {
        cb(err);
      });
    },
    updateUnitLocation: function(data, cb) {
      $http.post('/units/updateLocation', data).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    updateUnit: function(data, cb) {
      $http.post('/units/update', data).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    deleteUnit: function(unitId, cb) {
      $http.delete('/units/delete/' + unitId).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    findUnitById: function(unitId, cb) {
      $http.get('/units/get/' + unitId).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    updateUnitTable: function(data, cb) {
      $http.post('/units/updateunittable/', data).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    updateInfo: function(data, cb) {
      $http.post('/units/updateInfo/', data).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    }
  }
});

angular.module('index').service('StationService', function($http) {
  return {
    getStationsByUnitId: function(unitId, cb) {
      $http.get('/stations/' + unitId).then(function(res) {
        cb(res.data);
      });
    },
    createStation: function(data, cb) {
      $http.post('/stations/create', data).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    deleteStation: function(stationId, cb) {
      $http.delete('/stations/delete/' + stationId).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    updateInfo: function(data, cb) {
      $http.post('/stations/updateInfo/', data).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    updateStadionTable: function(data, cb) {
      $http.post('/stations/updateStationTb/', data).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    updateStationLocation: function(data, cb) {
      $http.post('/stations/updateLocation', data).then(function(res) {
        cb(null, res.data);
      }, function(err) {
        cb(err);
      });
    },
    getStationPaging: function(unitId, offset, limit, cb) {
      $http.get('/stations/paging/' + unitId + '/' + offset + '/' + limit).then(function(res) {
        cb(res.data);
      });
    },
  }
});
