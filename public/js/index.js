angular.module('index', ['ngRoute', 'ui.tree', 'leaflet-directive', 'ui.bootstrap', 'flow', 'ngFileUpload']);

angular.module('index').config(function($routeProvider, $logProvider, flowFactoryProvider) {
  $logProvider.debugEnabled(false);

  $routeProvider
    .when('/', {
      controller: 'indexCtrl',
      templateUrl: '/html/index/index.html'
    })
    .when('/table', {
      controller: 'tableCtrl',
      templateUrl: '/html/index/table.html'
    })
    .otherwise({
      redirectTo: '/'
    });

  flowFactoryProvider.defaults = {
    target: '/upload',
    permanentErrors: [404, 500, 501]
  };
  // You can also set default events:
  flowFactoryProvider.on('catchAll', function(event) {
    console.log(event)
  });
  // Can be used with different implementations of Flow.js
  // flowFactoryProvider.factory = fustyFlowFactory;

});
