module.exports = function(sequelize, DataTypes) {
  var Unit = sequelize.define('Unit', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    parentId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      hierarchy: true
    },
    longitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    latitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    avatar: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'unit',
    freezeTableName: true,
    timestamps: false,
    classMethods: {
      associate: function(models) {
        Unit.hasMany(models.Station);
      }
    }
  });

  return Unit;
};
