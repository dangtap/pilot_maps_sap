module.exports = function(sequelize, DataTypes) {
  var Station = sequelize.define('Station', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    unitId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    longitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    latitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    avatar: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'station',
    freezeTableName: true,
    timestamps: false,
    classMethods: {
      associate: function(models) {
        Station.belongsTo(models.Unit, {
          onDelete: 'CASCADE',
          foreignKey: 'unitId'
        });
      }
    }
  });

  return Station;
};
