var express = require('express');
var router = express.Router();
var sequelize = require('../models').sequelize;
var Unit = sequelize.import('../models/unit');
var Station = sequelize.import('../models/station');


router.get('/:unitId', function(req, res, next) {
  var unitId = req.params.unitId;
  Station.findAll({ where: { unitId: unitId } }).then(function(stations) {
    res.send(stations);
  }).catch(next);
});
router.get('/paging/:unitId/:offset/:limit', function(req, res, next) {
  var unitId = req.params.unitId;
  var offset = parseInt(req.params.offset);
  var limit = parseInt(req.params.limit);
  Station.findAll({
    where: {
      unitId: unitId
    },
    offset: offset,
    limit: limit
  }).then(function(stations) {
    res.send(stations);
  }).catch(next);
});
router.post('/create', function(req, res, next) {
  var station = req.body;
  Station.create({
    name: station.name,
    unitId: station.parent.id,
    longitude: station.location.long,
    latitude: station.location.lat
  }).then(function(station) {
    res.send(station);
  }).catch(function() {
    res.status(400);
    next();
  });
});

router.delete('/delete/:id', function(req, res, next) {
  var stationId = req.params.id;
  Station.destroy({
    where: {
      id: stationId
    }
  }).then(function() {
    res.sendStatus(200);
  }).catch(next);
});

router.post('/updateInfo', function(req, res, next) {
  var station = req.body;
  Station.findOne({
    where: {
      id: station.id
    }
  }).then(function(stationDB) {
    stationDB.update({
      name: station.name,
      unitId: station.unitId
    }).then(function(updatedStation) {
      Unit.findOne({
        where: {
          id: updatedStation.unitId
        },
        include: [{
          model: Unit,
          as: 'ancestors'
        }],
        order: [
          [{
            model: Unit,
            as: 'ancestors'
          }, 'hierarchyLevel']
        ]
      }).then(function(unit) {
        updatedStation.dataValues.unit = unit.dataValues;
        res.send(updatedStation);
      }).catch(next);
    });
  });
});
router.post('/updateStationTb', function(req, res, next) {
  var station = req.body;
  console.log(station);
  Station.findOne({
    where: {
      id: station.id
    }
  }).then(function(stationDB) {
    // console.log(stationDB);
    stationDB.update({
      name: station.name,
      unitId: station.unitId,
      longitude: station.longitude,
      latitude: station.latitude
    }).then(function(updatedStation) {
      res.send(updatedStation);
    }).catch(function() {});
  });
});
router.post('/updateLocation', function(req, res, next) {
  var station = req.body;
  Station.findOne({
    where: {
      id: station.id
    }
  }).then(function(stationDB) {
    stationDB.update({
      longitude: station.lng,
      latitude: station.lat
    }).then(function(updatedStation) {
      res.send(updatedStation);
    });
  });
});

module.exports = router;
