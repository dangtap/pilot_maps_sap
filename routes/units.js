var express = require('express');
var router = express.Router();
var sequelize = require('../models').sequelize;
var Unit = sequelize.import('../models/unit');
var multer = require('multer');
var upload = multer({ dest: 'public/images/unit' });

router.get('/', function(req, res, next) {
  Unit.findAll({
    hierarchy: true
  }).then(function(units) {
    res.send(units);
  }).catch(next);
});
router.get('/paging/:offset/:limit', function(req, res, next) {
  var offset = parseInt(req.params.offset);
  var limit = parseInt(req.params.limit);
  Unit.findAll({ offset: offset, limit: limit
  }).then(function(units) {
    res.send(units);
  }).catch(next);
});

router.get('/noHierarchy', function(req, res, next) {
  Unit.findAll({}).then(function(units) {
    res.send(units);
  }).catch(next);
});

router.get('/children/:id', function(req, res, next) {
  var parentId = parseInt(req.params.id) || null;
  Unit.findAll({
    where: {
      parentId: parentId
    },
    include: [{
      model: Unit,
      as: 'children'
    }]
  }).then(function(units) {
    res.send(units);
  }).catch(next);
});

router.get('/search/:term', function(req, res, next) {
  var term = req.params.term;
  Unit.findAll({
    where: {
      name: {
        $like: "%" + term + "%"
      }
    },
    include: [{
      model: Unit,
      as: 'ancestors'
    }],
    order: [
      [{
        model: Unit,
        as: 'ancestors'
      }, 'hierarchyLevel']
    ]
  }).then(function(units) {
    res.send(units);
  }).catch(next);
});

router.post('/create', upload.single('avatar'), function(req, res, next) {
  var unit = req.body;
  var path = req.file ? "/images/unit/" + req.file.filename : null;
  Unit.create({
    name: unit.name,
    parentId: unit.parentId,
    longitude: unit.location.lng,
    latitude: unit.location.lat,
    avatar: path
  }).then(function(unit) {
    Unit.findOne({
      where: {
        id: unit.id
      },
      include: [{
        model: Unit,
        as: 'ancestors'
      }],
      order: [
        [{
          model: Unit,
          as: 'ancestors'
        }, 'hierarchyLevel']
      ]
    }).then(function(unit) {
      res.send(unit);
    }).catch(next);
  }).catch(function() {
    res.status(400);
    next();
  });
});

router.post('/updateInfo', function(req, res, next) {
  var unit = req.body;
  Unit.findOne({
    where: {
      id: unit.id
    }
  }).then(function(unitDB) {
    unitDB.update({
      name: unit.name,
      parentId: unit.parentId || null
    }).then(function(updatedUnit) {
      Unit.findOne({
        where: {
          id: updatedUnit.id
        },
        include: [{
          model: Unit,
          as: 'ancestors'
        }],
        order: [
          [{
            model: Unit,
            as: 'ancestors'
          }, 'hierarchyLevel']
        ]
      }).then(function(resUnit) {
        res.send(resUnit);
      }).catch(next);
    });
  });
});

router.post('/updateLocation', function(req, res, next) {
  var unit = req.body;
  Unit.findOne({
    where: {
      id: unit.id
    }
  }).then(function(unitDB) {
    unitDB.update({
      longitude: unit.lng,
      latitude: unit.lat
    }).then(function(updatedUnit) {
      res.send(updatedUnit);
    });
  });
});
router.post('/updateunittable', function(req, res, next) {
  var unit = req.body;
  Unit.findOne({
    where: {
      id: unit.id
    }
  }).then(function(unitDB) {
    unitDB.update({
      name: unit.name,
      parentId: unit.parentId || null,
      longitude: unit.longitude,
      latitude: unit.latitude
    }).then(function(updatedUnit) {
      res.send(updatedUnit);
    }).catch(function() {
      // tìm lỗi
    });
  }).catch(next);
});
router.post('/update', function(req, res, next) {
  var unit = req.body;
  Unit.findOne({
    where: {
      id: unit.id
    }
  }).then(function(unitDB) {
    unitDB.update({
      name: unit.name,
      parentId: unit.parentId,
      avatar: unit.avatar
    }).then(function(updatedUnit) {
      res.send(updatedUnit);
    }).catch(next);
  }).catch(next);
});

router.delete('/delete/:id', function(req, res, next) {
  var unitId = parseInt(req.params.id);
  Unit.destroy({
    where: {
      id: unitId
    }
  }).then(function() {
    res.sendStatus(200);
  }).catch(next);
});

router.get('/get/:id', function(req, res, next) {
  Unit.findOne({ where: { id: req.params.id } }).then(function(unit) {
    res.send(unit);
  });
});


module.exports = router;
