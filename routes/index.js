var express = require('express');
var router = express.Router();
var sequelize = require('../models').sequelize;
var Unit = sequelize.import('../models/unit');
var Station = sequelize.import('../models/station');


router.get('/stations/:unitId', function(req, res, next) {
  var unitId = req.params.unitId;
  Station.findAll({ where: { unitId: unitId } }).then(function(stations) {
    res.send(stations);
  }).catch(next);
});

module.exports = router;
